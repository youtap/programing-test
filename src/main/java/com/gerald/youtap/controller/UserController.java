package com.gerald.youtap.controller;

import com.gerald.youtap.response.UserResponse;
import com.gerald.youtap.service.UserService;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/",
        produces = MediaType.APPLICATION_JSON_VALUE,
        headers = "Accept=application/json")
@Api(tags = {"User Controller"}, consumes = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("getusercontacts")
    public ResponseEntity<UserResponse> getUser(@RequestParam(value = "userId", required = false) Long userId,
                                                @RequestParam(value = "username", required = false) String username) {
        return new ResponseEntity<>(userService.getUser(userId, username), HttpStatus.OK);
    }
}

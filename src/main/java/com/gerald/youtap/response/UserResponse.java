package com.gerald.youtap.response;

import lombok.Data;

@Data
public class UserResponse {
    private int id;
    private String email;
    private String phone;
}

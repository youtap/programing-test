package com.gerald.youtap.dto;

import lombok.Data;

@Data
public class CompanyDto {
    private String name;
    private String catchPhrase;
    private String bs;
}

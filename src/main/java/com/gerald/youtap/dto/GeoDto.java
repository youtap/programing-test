package com.gerald.youtap.dto;

import lombok.Data;

@Data
public class GeoDto {
    private String lat;
    private String lng;
}

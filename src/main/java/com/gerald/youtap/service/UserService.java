package com.gerald.youtap.service;

import com.gerald.youtap.response.UserResponse;

public interface UserService {

    UserResponse getUser(Long userId, String username);
}

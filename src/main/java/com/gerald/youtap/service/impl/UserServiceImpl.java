package com.gerald.youtap.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gerald.youtap.dto.UserDto;
import com.gerald.youtap.response.UserResponse;
import com.gerald.youtap.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    //url for user service
    @Value("${third-party.url}")
    private String url;

    //rest template for http requests
    RestTemplate restTemplate = new RestTemplate();
    private final ObjectMapper objectMapper;

    public UserServiceImpl(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public UserResponse getUser(Long userId, String username) {
        List<UserDto> list = null;
        UserResponse userResponse = new UserResponse();
        userResponse.setId(-1);
        try {
            //check for null in id or username
            if (userId == null && username == null) {
                return userResponse;
            }
            ResponseEntity<Object> response = restTemplate.getForEntity(url, Object.class);

            if (!response.hasBody()) {
                return userResponse;
            }

            String jsonString = objectMapper.writeValueAsString(response.getBody());
            TypeReference<List<UserDto>> typeReference = new TypeReference<List<UserDto>>() {
            };
            List<UserDto> userDtoList = objectMapper.readValue(jsonString, typeReference);

            //lookup for record using username and id
            //filter list using parameters
            if (userId != null && username != null) {
              list= userDtoList.stream().filter(user -> user.getId().equals(userId) &&
                        user.getUsername().equalsIgnoreCase(username)).collect(Collectors.toList());
                if (list.size() == 0) {
                    return userResponse;
                }
                return setRecord(list.get(0));

            } else if (userId != null) {
                list=userDtoList.stream().filter(user -> user.getId().equals(userId)).collect(Collectors.toList());
                if (list.size() == 0) {
                    return userResponse;
                }
                return setRecord(list.get(0));

            } else {
                list = userDtoList.stream().filter(user -> user.getUsername().equals(username)).collect(Collectors.toList());
                if (list.size() == 0) {
                    return userResponse;
                }
                return setRecord(list.get(0));
            }
        } catch (Exception e) {
            log.error("ERROR " + e.getMessage());
        }
        return userResponse;
    }

    private UserResponse setRecord(UserDto userDto){
        UserResponse userResponse = new UserResponse();
        userResponse.setId(userDto.getId().intValue());
        userResponse.setEmail(userDto.getEmail());
        userResponse.setPhone(userDto.getPhone());
        return userResponse;
    }
}
